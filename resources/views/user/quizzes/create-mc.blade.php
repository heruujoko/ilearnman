@extends('user.app')

@section('content')	
	<div class="container libraries">
		<div class="row">
			<div class="col-sm-4 col-md-2 hidden-xs">
				@include('user.global.sidebars._sidebar-left')
			</div>
			<div class="col-md-10">		
				@if (count($errors) > 0)
			    <div class="alert alert-danger">
		        <ul>
		        	@foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
	            @endforeach
		        </ul>
			    </div>
				@endif

				<div class="profile-form panel panel-default">
					<header class="panel-heading">
						<h2 class="panel-title text-bold">{{ $page_title }}</h2>
					</header>
					<div class="panel-body">
						{!! Form::open(['route' => ['quizzes.mc.store', $quiz->id], 'class' => 'multiple-choice-form']) !!}
							@include('user.quizzes._form-mc')
							<div class="col-md-offset-1 form-submit"> 
								{!! Form::submit('Simpan', ['class'=>'btn btn-flat btn-success', 'id' => 'submit-mc']) !!}
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ url('/assets/js/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ url('/assets/js/ckeditor/config.js') }}"></script>
	<script src="{{ url('/assets/js/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js') }}"></script>
	<script type="text/javascript">
		CKEDITOR.editorConfig = function( config ) {
			// Define changes to default configuration here. For example:
			// config.language = 'fr';
			// config.uiColor = '#AADC6E';
			config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
			config.allowedContent = true;
			config.toolbar = [
				{ name: 'about', items: [ 'About' ] },
				{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
				{ name: 'editing', items: [ 'Scayt' ] },
				{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
				{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
				{ name: 'tools', items: [ 'Maximize' ] },
				{ name: 'document', items: [ 'Source' ] },
				'/',
				{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
				{ name: 'styles', items: [ 'Styles', 'Format' ] },
				{ name: 'wiris', items: ['ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry']}

			];
		};
		CKEDITOR.replaceClass = 'quiz-editor';
	</script>
@endsection